C
C     @(#)  412.1 date 6/11/92 server.inc 
C
C
C     Filename    : server.inc
C     Version     : 412.1
C     Retrieved   : 92/06/12 15:15:44
C     Last change : 92/06/11 14:40:46
C
C     Copyright : Practical Technology Limited  
C     File :- server.inc
C
C     DAXCAD FORTRAN 77 Include file
C
C
C     |-----------------------------------------------------------------|
C
C	include file for the Daxcad server
C
      LOGICAL SERVER
      COMMON/SERVA/ SERVER

