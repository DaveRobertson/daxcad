C
C     @(#)  412.1 date 6/11/92 rcv2.inc 
C
C
C     Filename    : rcv2.inc
C     Version     : 412.1
C     Retrieved   : 92/06/12 15:15:40
C     Last change : 92/06/11 14:39:27
C
C     Copyright : Practical Technology Limited  
C     File :- rcv2.inc
C
C     DAXCAD FORTRAN 77 Include file
C
C
C     |-----------------------------------------------------------------|
C
C
C
C2    This common block contains all the character elements
C2    associated with analysis of a computervision CADDS3
C2    database file.
C
      CHARACTER TAGNAM*4
C
      COMMON/RCV2/TAGNAM
C
C     end of this common block
C
