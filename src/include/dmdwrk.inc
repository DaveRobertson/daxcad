C
C     @(#)  412.1 date 6/11/92 dmdwrk.inc 
C
C
C     Filename    : dmdwrk.inc
C     Version     : 412.1
C     Retrieved   : 92/06/12 15:15:08
C     Last change : 92/06/11 14:28:52
C
C     Copyright : Practical Technology Limited  
C     File :- dmdwrk.inc
C
C     DAXCAD FORTRAN 77 Include file
C
C
C     |-----------------------------------------------------------------|
C
C
C     This include file contains the declaration of
C     a common block for use as general workspace
C     in the construction of DIMMOD DATA.
C
      REAL CWORK(6,20)
C
      COMMON/DMDAT/CWORK
C
