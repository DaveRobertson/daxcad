C
C     @(#)  412.1 date 6/11/92 ptemps.inc 
C
C
C     Filename    : ptemps.inc
C     Version     : 412.1
C     Retrieved   : 92/06/11 14:39:04
C     Last change : 92/06/11 14:39:03
C
C     Copyright : Practical Technology International Limited  
C     File :- ptemps.inc
C
C     DAXCAD FORTRAN 77 Source file
C
C
C
C2    This include file contains the
C2    definitions required for PROPERTIES
C2    control and processing.
C
      INTEGER*4 NUMCOL,TCDAT(8,20)
      CHARACTER*80 TCON(6,20)
      CHARACTER*80 TYPES(10),SOURCE(10),JUST(10)
C
C
