C
C     @(#)  412.1 date 6/11/92 tdmdat.inc 
C
C
C     Filename    : tdmdat.inc
C     Version     : 412.1
C     Retrieved   : 92/06/12 15:15:49
C     Last change : 92/06/11 14:41:36
C
C     Copyright : Practical Technology Limited  
C     File :- tdmdat.inc
C
C     DAXCAD FORTRAN 77 Include file
C
C
C     |-----------------------------------------------------------------|
C
C
C
C     This holds the temporary values for text parameters.
C     Used in the change dimension module.
C
      REAL TDTHGT,TDTWDT,TALNG,TAWDT,TGAPL,TEXTL,TDTOFF
C
      COMMON /TDMDAT/TDTHGT,TDTWDT,TALNG,TAWDT,TGAPL,TEXTL,TDTOFF
