C
C
C     @(#)  412.1 date 6/11/92 abs.inc 
C
C
C     Filename    : abs.inc
C     Version     : 412.1
C     Retrieved   : 92/06/12 15:14:54
C     Last change : 92/06/11 14:22:57
C
C     Copyright : Practical Technology Limited  
C     File :- abs.inc
C
C     DAXCAD FORTRAN 77 Include file
C
C
C     |-----------------------------------------------------------------|
C
      REAL ABSX,ABSY,ABSZ
      COMMON /LABSP/ABSX,ABSY,ABSZ
