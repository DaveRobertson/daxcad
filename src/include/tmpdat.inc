C
C     @(#)  412.1 date 6/11/92 tmpdat.inc 
C
C
C     Filename    : tmpdat.inc
C     Version     : 412.1
C     Retrieved   : 92/06/12 15:15:50
C     Last change : 92/06/11 14:42:08
C
C     Copyright : Practical Technology Limited  
C     File :- tmpdat.inc
C
C     DAXCAD FORTRAN 77 Include file
C
C
C     |-----------------------------------------------------------------|
C
C
C 
       REAL RWORK2(20),RFDAT(20)
C
       COMMON /TMPDAT/RWORK2,RFDAT
C
