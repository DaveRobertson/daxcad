C
C     @(#)  412.1 date 6/11/92 dhr.inc 
C
C
C     Filename    : dhr.inc
C     Version     : 412.1
C     Retrieved   : 92/06/12 15:15:06
C     Last change : 92/06/11 14:28:18
C
C     Copyright : Practical Technology Limited  
C     File :- dhr.inc
C
C     DAXCAD FORTRAN 77 Include file
C
C
C     |-----------------------------------------------------------------|
C
C
C     Additianal globals for prep
C	
      INTEGER*2 EDSTS
      LOGICAL DHRST

      COMMON /DHR/DHRST,EDSTS

