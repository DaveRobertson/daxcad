C
C     @(#)  412.1 date 6/11/92 dcross.inc 
C
C
C     Filename    : dcross.inc
C     Version     : 412.1
C     Retrieved   : 92/06/12 15:15:04
C     Last change : 92/06/11 14:27:51
C
C     Copyright : Practical Technology Limited  
C     File :- dcross.inc
C
C     DAXCAD FORTRAN 77 Include file
C
C
C     |-----------------------------------------------------------------|
C
C     Flag tells if crosses are allowed or not.
      LOGICAL DCROS
      COMMON/DCR/DCROS
