C
C     @(#)  412.1 date 6/11/92 punch.inc 
C
C
C     Filename    : punch.inc
C     Version     : 412.1
C     Retrieved   : 92/06/12 15:15:38
C     Last change : 92/06/11 14:39:08
C
C     Copyright : Practical Technology Limited  
C     File :- punch.inc
C
C     DAXCAD FORTRAN 77 Include file
C
C
C     |-----------------------------------------------------------------|
C
C
      CHARACTER*255 CHR
      CHARACTER*60 ERRSTR,FILNM*100
      CHARACTER*1 ASCII,BINARY,READ,WRITE,DIREC,FTYPE
      LOGICAL WRTOK,BATCH
      INTEGER*4 IDNUM,ERRNO,RECL,BLOCKF,MP
      INTEGER*4 IN,CURPOS,BUFLEN
      INTEGER*2 STRIDO,STRIDI,FILID
      INTEGER*2 NBLANK,SIOSPE,SIOPAR,SIOSTO,SIODAT

      COMMON /TYPE4/IDNUM,ERRNO,RECL,BLOCKF,WRTOK,BATCH,IN,MP

      COMMON /TYPE2/CURPOS,BUFLEN,STRIDI,STRIDO,FILID,
     +              NBLANK,SIOSPE,SIOPAR,SIOSTO,SIODAT

      COMMON /ESTR/CHR,ERRSTR,FILNM,ASCII,BINARY,READ,
     +                          WRITE,DIREC,FTYPE

