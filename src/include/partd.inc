C
C     @(#)  412.1 date 6/11/92 partd.inc 
C
C
C     Filename    : partd.inc
C     Version     : 412.1
C     Retrieved   : 92/06/12 15:15:32
C     Last change : 92/06/11 14:37:19
C
C     Copyright : Practical Technology Limited  
C     File :- partd.inc
C
C     DAXCAD FORTRAN 77 Include file
C
C
C     |-----------------------------------------------------------------|
C
C
      INTEGER*2 PDFILE(32000),PDPTR
      COMMON/PDATA/PDFILE,PDPTR
