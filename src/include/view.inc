C
C     @(#)  412.1 date 6/11/92 view.inc 
C
C
C     Filename    : view.inc
C     Version     : 412.1
C     Retrieved   : 92/06/12 15:15:52
C     Last change : 92/06/11 14:42:52
C
C     Copyright : Practical Technology Limited  
C     File :- view.inc
C
C     DAXCAD FORTRAN 77 Include file
C
C
C     |-----------------------------------------------------------------|
C
C
C
      REAL VDIST,VTHETA,VPHI
      INTEGER*4 VOPT1,VOPT2
C
      COMMON /VVIEW/VDIST,VTHETA,VPHI,VOPT1,VOPT2
C
