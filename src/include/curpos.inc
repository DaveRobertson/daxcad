C
C     @(#)  412.1 date 6/11/92 curpos.inc 
C
C
C     Filename    : curpos.inc
C     Version     : 412.1
C     Retrieved   : 92/06/12 15:15:01
C     Last change : 92/06/11 14:25:56
C
C     Copyright : Practical Technology Limited  
C     File :- curpos.inc
C
C     DAXCAD FORTRAN 77 Include file
C
C
C     |-----------------------------------------------------------------|
C
C

      REAL SCPX,SCPY,WCPX,WCPY
      COMMON /SCURSR/SCPX,SCPY
      COMMON /WCURSR/WCPX,WCPY

