C
C     @(#)  412.1 date 6/11/92 daxports.inc 
C
C
C     Filename    : daxports.inc
C     Version     : 412.1
C     Retrieved   : 92/06/12 15:15:04
C     Last change : 92/06/11 14:27:47
C
C     Copyright : Practical Technology Limited  
C     File :- daxports.inc
C
C     DAXCAD FORTRAN 77 Include file
C
C
C     |-----------------------------------------------------------------|
C
      INTEGER*4 MAPVP
      INTEGER*4 OBS_LIST(MAXVP,MAXVP)
      LOGICAL VPONSC(MAXVP)
C
      PARAMETER (MAXVP = 5)



