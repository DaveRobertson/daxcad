C
C     @(#)  412.1 date 6/11/92 wildc.inc 
C
C
C     Filename    : wildc.inc
C     Version     : 412.1
C     Retrieved   : 92/06/12 15:15:55
C     Last change : 92/06/11 14:43:36
C
C     Copyright : Practical Technology Limited  
C     File :- wildc.inc
C
C     DAXCAD FORTRAN 77 Include file
C
C
C     |-----------------------------------------------------------------|
C
C
C      this include file contains the different wildcards used by
C      the operating systems.
      CHARACTER WILDC(20)*6,FILCHR*1
      COMMON /WW1/WILDC,FILCHR
