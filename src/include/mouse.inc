C
C     @(#)  412.1 date 6/11/92 mouse.inc 
C
C
C     Filename    : mouse.inc
C     Version     : 412.1
C     Retrieved   : 92/06/12 15:15:26
C     Last change : 92/06/11 14:35:51
C
C     Copyright : Practical Technology Limited  
C     File :- mouse.inc
C
C     DAXCAD FORTRAN 77 Include file
C
C
C     |-----------------------------------------------------------------|
C
C

      INTEGER*4 XC,YC,OC,OX,OY,XCO,YCO,INCX,INCY

      COMMON /MSE1/XC,YC,OC,OX,OY,XCO,YCO,INCX,INCY
