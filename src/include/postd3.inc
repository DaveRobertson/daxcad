C
C     @(#)  412.1 date 6/11/92 postd3.inc 
C
C
C     Filename    : postd3.inc
C     Version     : 412.1
C     Retrieved   : 92/06/12 15:15:34
C     Last change : 92/06/11 14:38:21
C
C     Copyright : Practical Technology Limited  
C     File :- postd3.inc
C
C     DAXCAD FORTRAN 77 Include file
C
C
C     |-----------------------------------------------------------------|
C
C
C     
C2    This include file contains declarations
C2    for use within post-processor programs
C
      REAL TLDIAM,LASTF,THISF,NEXTF,NIBINC,LNBINC
      INTEGER GCHARS,TCHARS,DCHARS,ECHARS,MCHARS,LMOTN,LCIRQ,LDIMS
      LOGICAL GFUN,TFUN,DFUN,EFUN,MFUN,IJFUN,RFUN,FEDON,CIRCON,RAPON,
     +        PNCHON,FFXTON,NIBBLE,SCRBON,SCRBTR,TAPON,OPSKP,REVY,TLDIAM,
      COMMON /DATA3/GFUN,TFUN,DFUN,EFUN,MFUN,IJFUN,RFUN,LASTF,THISF,
     +       NEXTF,FEDON,CIRCON,RAPON,PNCHON,FFXTON,NIBBLE,SCRBON,
     +       SCRBTR,TAPON,OPSKP,REVY,NIBINC,LNBINC,GCHARS,TCHARS,
     +       DCHARS,ECHARS,MCHARS,LMOTN,LCIRQ,LDIM
C
C
