C
C     @(#)  412.1 date 6/11/92 clipper.inc 
C
C
C     Filename    : clipper.inc
C     Version     : 412.1
C     Retrieved   : 92/06/12 15:14:58
C     Last change : 92/06/11 14:25:20
C
C     Copyright : Practical Technology Limited  
C     File :- clipper.inc
C
C     DAXCAD FORTRAN 77 Include file
C
C
C     |-----------------------------------------------------------------|
C
C
      LOGICAL CLIPED
      COMMON/CLIP/CLIPED
