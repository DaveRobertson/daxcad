C
C     @(#)  412.1 date 6/11/92 shadow.inc 
C
C
C     Filename    : shadow.inc
C     Version     : 412.1
C     Retrieved   : 92/06/12 15:15:45
C     Last change : 92/06/11 14:40:48
C
C     Copyright : Practical Technology International Limited  
C     File :- shadow.inc
C
C     DAXCAD FORTRAN 77 Include file
C
C
C     Shadow attributes
C
C

      INTEGER*4 SHADAT,SHADBT
C
      COMMON/SHADI4/SHADAT,SHADBT
