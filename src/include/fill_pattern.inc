C
C     @(#)  412.1 date 6/11/92 fill_pattern.inc 
C
C
C     Filename    : fill_pattern.inc
C     Version     : 412.1
C     Retrieved   : 92/06/12 15:15:12
C     Last change : 92/06/11 14:29:53
C
C     Copyright : Practical Technology Limited  
C     File :- fill_pattern.inc
C
C     DAXCAD FORTRAN 77 Include file
C
C
C     |-----------------------------------------------------------------|
C
      INTEGER*4 FILL_BITMAP
      INTEGER*4 BACK_BITMAP

      COMMON/FILL_PATTERN/ FILL_BITMAP,BACK_BITMAP
