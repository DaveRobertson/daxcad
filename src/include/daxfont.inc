C
C     @(#)  412.1 date 6/11/92 daxfont.inc 
C
C
C     Filename    : daxfont.inc
C     Version     : 412.1
C     Retrieved   : 92/06/12 15:15:04
C     Last change : 92/06/11 14:27:39
C
C     Copyright : Practical Technology Limited  
C     File :- daxfont.inc
C
C     DAXCAD FORTRAN 77 Include file
C
C
C     |-----------------------------------------------------------------|
C
C
      INTEGER*4 FONT1(2000)
      COMMON /TEXTFONT/FONT1
C
