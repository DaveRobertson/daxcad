C
C     @(#)  412.1 date 6/11/92 rrn.inc 
C
C
C     Filename    : rrn.inc
C     Version     : 412.1
C     Retrieved   : 92/06/12 15:15:42
C     Last change : 92/06/11 14:40:12
C
C     Copyright : Practical Technology Limited  
C     File :- rrn.inc
C
C     DAXCAD FORTRAN 77 Include file
C
C
C     |-----------------------------------------------------------------|
C
C
c      This contains the root revision number for PREP
      REAL OROTRV
      COMMON /RRN/OROTRV
C
