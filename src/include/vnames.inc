C
C     @(#)  412.1 date 6/11/92 vnames.inc 
C
C
C     Filename    : vnames.inc
C     Version     : 412.1
C     Retrieved   : 92/06/12 15:15:53
C     Last change : 92/06/11 14:43:04
C
C     Copyright : Practical Technology Limited  
C     File :- vnames.inc
C
C     DAXCAD FORTRAN 77 Include file
C
C
C     |-----------------------------------------------------------------|
C
C     This include file holds the gubbins for viewprt names 
C
      CHARACTER*10 VPNAME(5),VPUDEF

      COMMON/VIEWPC/ VPNAME,VPUDEF
