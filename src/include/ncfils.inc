C
C     @(#)  412.1 date 6/11/92 ncfils.inc 
C
C
C     Filename    : ncfils.inc
C     Version     : 412.1
C     Retrieved   : 92/06/12 15:15:29
C     Last change : 92/06/11 14:36:19
C
C     Copyright : Practical Technology Limited  
C     File :- ncfils.inc
C
C     DAXCAD FORTRAN 77 Include file
C
C
C     |-----------------------------------------------------------------|
C
C
      INTEGER BLKNUM,BLKINC,CLUNIT,PPUNIT,PTUNIT,SUUNIT,TDUNIT
C
      COMMON /NCD4/BLKNUM,BLKINC,CLUNIT,PPUNIT,PTUNIT,SUUNIT,TDUNIT
C
C
      CHARACTER PRTNUM*80
C
      COMMON /NCD6/PRTNUM
