C
C     @(#)  412.1 date 6/11/92 viewmap.inc 
C
C
C     Filename    : viewmap.inc
C     Version     : 412.1
C     Retrieved   : 92/06/12 15:15:52
C     Last change : 92/06/11 14:42:57
C
C     Copyright : Practical Technology Limited  
C     File :- viewmap.inc
C
C     DAXCAD FORTRAN 77 Include file
C
C
C     |-----------------------------------------------------------------|
C
C
      INTEGER*2 BMSIZ(2),ATTRB,MAPBM,HIPLAN
      COMMON/VIEWN/ BMSIZ,ATTRB,MAPBM,HIPLAN
