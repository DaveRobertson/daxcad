C
C     @(#)  412.1 date 6/11/92 drgbar.inc 
C
C
C     Filename    : drgbar.inc
C     Version     : 412.1
C     Retrieved   : 92/06/12 15:15:08
C     Last change : 92/06/11 14:28:57
C
C     Copyright : Practical Technology Limited  
C     File :- drgbar.inc
C
C     DAXCAD FORTRAN 77 Include file
C
C
C     |-----------------------------------------------------------------|
C
      INTEGER*4 BRXMAX,BRXMIN,BRYMAX,BRYMIN
      COMMON/DRGBAR/BRXMAX,BRXMIN,BRYMAX,BRYMIN
