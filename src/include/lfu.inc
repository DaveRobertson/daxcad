C
C     @(#)  412.1 date 6/11/92 lfu.inc 
C
C
C     Filename    : lfu.inc
C     Version     : 412.1
C     Retrieved   : 92/06/12 15:15:22
C     Last change : 92/06/11 14:33:33
C
C     Copyright : Practical Technology Limited  
C     File :- lfu.inc
C
C     DAXCAD FORTRAN 77 Include file
C
C
C     |-----------------------------------------------------------------|
C
C
      LOGICAL LFU(40)
      COMMON/FNU/ LFU
