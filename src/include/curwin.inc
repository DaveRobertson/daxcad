C
C     @(#)  412.1 date 6/11/92 curwin.inc 
C
C
C     Filename    : curwin.inc
C     Version     : 412.1
C     Retrieved   : 92/06/12 15:15:02
C     Last change : 92/06/11 14:26:06
C
C     Copyright : Practical Technology Limited  
C     File :- curwin.inc
C
C     DAXCAD FORTRAN 77 Include file
C
C
C     |-----------------------------------------------------------------|
C
C
C
      REAL XCURS,YCURS,XWIDTH,YWIDTH,WIDTHL
      COMMON /CURWIN/ XCURS,YCURS,XWIDTH,YWIDTH,WIDTHL
C
