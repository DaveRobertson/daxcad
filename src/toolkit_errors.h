/*
     @(#)  412.1 date 6/11/92 toolkit_errors.h 

     Filename    : toolkit_errors.h
     Version     : 412.1
     Retrieved   : 92/06/11 14:42:16
     Last change : 92/06/11 14:42:15

     Copyright : Practical Technology Limited  
     File :- toolkit_errors.h

     C include file

     Toolkit error codes and error messages

*/

#define TK_SUCCESS                  0
#define TK_NO_WINDOW               -1
#define TK_WINDOW_NOT_ACTIVE       -2
#define TK_CANNOT_LOAD_FONT        -3
#define TK_INVALID_WINDOW          -4
#define TK_CANNOT_UNLOAD_FONT      -5
#define TK_INVALID_FONT            -6
#define TK_INVALID_JUST            -7
#define TK_INVALID_STRING          -8
#define TK_NO_BUTTON               -9
#define TK_BUTTON_NOT_ACTIVE       -10
#define TK_TEXTWINDOW_NOT_ACTIVE   -11
#define TK_TEXTWINDOW_SIZE         -12
#define TK_INVALID_TEXTWINDOW      -13
#define TK_CANNOT_CREATE_SCROLL    -14
#define TK_INVALID_SCROLLBAR       -15
#define TK_SCROLLBAR_NOT_ACTIVE    -16
#define TK_INVALID_RESOURCE        -17
