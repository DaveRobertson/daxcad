/* SCCS id Keywords             @(#)  412.1 date 6/11/92 misomex_codes.h   */

/*
 *
 *
 *        Filename    : misomex_codes.h
 *        Version     : 412.1
 *
 *        Copyright : Practical Technology Limited
 *
 *        Include file for MISOMEX opcodes
 *
 * 
 *
*/

#define MX_PLOTTER_SELECT 0
#define MX_BUFFER_REPEAT  1
#define MX_SEND_BUFFER    2
#define MX_DRAW_ARC       3
#define MX_DRAW_CIRCLE    4
#define MX_DRAW_ELLIPSE   5
#define MX_GEN_CURVE      6
#define MX_RESOLUTION     7
#define MX_PAUSE          8
#define MX_MAX_CUT        9
#define MX_PEN_DWELL      10
#define MX_SET_LINE       11
#define MX_ACCEL_DIST     12
#define MX_SEND_POS       13
#define MX_ABS_SYS        14
#define MX_PEN_DOWN       15
#define MX_HOME           16
#define MX_INIT_DELAY     17
#define MX_SELECT_LINE    18
#define MX_ORIGIN         19
#define MX_PEN_TYPE       20
#define MX_REL_SYS        21
#define MX_TEST           22
#define MX_PEN_UP         23
#define MX_VELOCITY       24
#define MX_PLOTTER_END    25
#define MX_TEXT           26

