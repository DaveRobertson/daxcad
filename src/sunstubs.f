
C
C     @(#)  412.1 date 6/11/92 sunstubs.f 
C
C
C     Filename    : sunstubs.f
C     Version     : 412.1
C     Retrieved   : 92/06/12 15:47:19
C     Last change : 92/06/11 14:41:29
C
C     Copyright : Practical Technology International Limited  
C     File :- sunstubs.f
C
C     DAXCAD FORTRAN 77 Source file
C
C     Specific sun stub routines for items not in main version
C
C
C
C     Functions and subroutines index:-
C
C     |-----------------------------------------------------------------|
C
      SUBROUTINE INITMS()
      END
      SUBROUTINE SIOCLS()
      END
      SUBROUTINE MOUSE()
      END
      SUBROUTINE CLOSMS()
      END
      SUBROUTINE RSTSIO()
      END

