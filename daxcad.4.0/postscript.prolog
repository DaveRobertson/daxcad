/m {moveto}bind def
/l {lineto}bind def
/n {newpath}bind def
/s {stroke}bind def
/sw {setlinewidth}bind def
/sc {setlinecap}bind def


/BuildCharDict 10 dict def
/ExampleFont 128 dict def
ExampleFont begin
  /FontType 3 def
  /FontMatrix [.03333 0 0 .03333 0 0] def
  /FontBBox [-3 -18 23 33] def
  /Encoding 256 array def
  0 1 255 {Encoding exch /.notdef put} for
  Encoding (A) 0 get /_A put
  Encoding (B) 0 get /_B put
  Encoding (C) 0 get /_C put
  Encoding (D) 0 get /_D put
  Encoding (E) 0 get /_E put
  Encoding (F) 0 get /_F put
  Encoding (G) 0 get /_G put
  Encoding (H) 0 get /_H put
  Encoding (I) 0 get /_I put
  Encoding (J) 0 get /_J put
  Encoding (K) 0 get /_K put
  Encoding (L) 0 get /_L put
  Encoding (M) 0 get /_M put
  Encoding (N) 0 get /_N put
  Encoding (O) 0 get /_O put
  Encoding (P) 0 get /_P put
  Encoding (Q) 0 get /_Q put
  Encoding (R) 0 get /_R put
  Encoding (S) 0 get /_S put
  Encoding (T) 0 get /_T put
  Encoding (U) 0 get /_U put
  Encoding (V) 0 get /_V put
  Encoding (W) 0 get /_W put
  Encoding (X) 0 get /_X put
  Encoding (Y) 0 get /_Y put
  Encoding (Z) 0 get /_Z put
  Encoding (0) 0 get /_0 put
  Encoding (1) 0 get /_1 put
  Encoding (2) 0 get /_2 put
  Encoding (3) 0 get /_3 put
  Encoding (4) 0 get /_4 put
  Encoding (5) 0 get /_5 put
  Encoding (6) 0 get /_6 put
  Encoding (7) 0 get /_7 put
  Encoding (8) 0 get /_8 put
  Encoding (9) 0 get /_9 put
  Encoding (a) 0 get /_a put
  Encoding (b) 0 get /_b put
  Encoding (c) 0 get /_c put
  Encoding (d) 0 get /_d put
  Encoding (e) 0 get /_e put
  Encoding (f) 0 get /_f put
  Encoding (g) 0 get /_g put
  Encoding (h) 0 get /_h put
  Encoding (i) 0 get /_i put
  Encoding (j) 0 get /_j put
  Encoding (k) 0 get /_k put
  Encoding (l) 0 get /_l put
  Encoding (m) 0 get /_m put
  Encoding (n) 0 get /_n put
  Encoding (o) 0 get /_o put
  Encoding (p) 0 get /_p put
  Encoding (q) 0 get /_q put
  Encoding (r) 0 get /_r put
  Encoding (s) 0 get /_s put
  Encoding (t) 0 get /_t put
  Encoding (u) 0 get /_u put
  Encoding (v) 0 get /_v put
  Encoding (w) 0 get /_w put
  Encoding (x) 0 get /_x put
  Encoding (y) 0 get /_y put
  Encoding (z) 0 get /_z put
  Encoding (!) 0 get /_! put
  Encoding (") 0 get /_" put
  Encoding (#) 0 get /_# put
  Encoding ($) 0 get /_$ put
  Encoding (%) 0 get /_percent put
  Encoding (&) 0 get /_& put
  Encoding (') 0 get /_' put
  Encoding (\() 0 get /_lparen put
  Encoding (\)) 0 get /_rparen put
  Encoding (*) 0 get /_* put
  Encoding (+) 0 get /_+ put
  Encoding (,) 0 get /_, put
  Encoding (-) 0 get /_- put
  Encoding (.) 0 get /_. put
  Encoding (/) 0 get /_slash put
  Encoding (:) 0 get /_: put
  Encoding (;) 0 get /_; put
  Encoding (<) 0 get /_lt put
  Encoding (=) 0 get /_= put
  Encoding (>) 0 get /_gt put
  Encoding (?) 0 get /_? put
  Encoding (@) 0 get /_@ put
  Encoding ([) 0 get /_lbracket put
  Encoding (\\) 0 get /_backslash put
  Encoding (]) 0 get /_rbracket put
  Encoding (^) 0 get /_^ put
  Encoding (_) 0 get /__ put
  Encoding (`) 0 get /_` put
  Encoding ({) 0 get /_lbrace put
  Encoding (|) 0 get /_| put
  Encoding (}) 0 get /_rbrace put
  Encoding (~) 0 get /_~ put

  /CharacterDefs 128 dict def
  CharacterDefs begin
    /.notdef {} def
    /_A { n 1 0 m 11 30 l 21 0 l 16 15 m 6 15 l 1 sw s } def
    /_B { n 1 0 m 1 30 l 14 30 l 17 29 l 19 28 l 20 26 l 21 24 l 21 21 l
	  20 19 l 19 17 l 17 16 l 14 15 l 1 15 l 1 0 m 14 0 l 17 1 l 19 2 l
	  20 4 l 21 6 l 21 9 l 20 11 l 19 13 l 17 14 l 14 15 l 1 sw s } def
    /_C { n 21 10 m 19 4 l 17 2 l 15 1 l 13 0 l 9 0 l 7 1 l 5 2 l 3 4 l
	  2 6 l 1 10 l 1 20 l 2 24 l 3 26 l 5 28 l 7 29 l 9 30 l 13 30 l
	  15 29 l 17 28 l 19 26 l 20 24 l 21 20 l 1 sw s } def
    /_D { n 1 0 m 1 30 l 11 30 l 15 29 l 17 28 l 19 26 l 20 24 l 21 20 l
	  21 10 l 20 6 l 19 4 l 17 2 l 15 1 l 11 0 l 1 0 l 1 sw s } def
    /_E { n 1 0 m 1 30 l 21 30 l 21 28 l 1 15 m 16 15 l 16 17 l 16 15 m
	  16 13 l 1 0 m 21 0 l 21 2 l 1 sw s } def
    /_F { n 1 0 m 1 30 l 21 30 l 21 28 l 1 15 m 16 15 l 16 17 l 16 15 m
	  16 13 l 1 sw s } def
    /_G { n 21 20 m 20 24 l 19 26 l 17 28 l 15 29 l 13 30 l 9 30 l 7 29 l
	  5 28 l 3 26 l 2 24 l 1 20 l 1 10 l 2 6 l 3 4 l 5 2 l 7 1 l 9 0 l
	  13 0 l 15 1 l 17 2 l 19 4 l 20 6 l 21 10 l 16 10 l 21 10 m 21 0 l
	  1 sw s } def
    /_H { n 1 0 m 1 30 l 21 0 m 21 30 l 21 15 m 1 15 l 1 sw s } def
    /_I { n 11 0 m 11 30 l 6 30 m 16 30 l 6 0 m 16 0 l 1 sw s } def
    /_J { n 1 5 m 2 3 l 4 1 l 6 0 l 9 0 l 11 1 l 13 3 l 14 5 l 14 30 l
	  9 30 m 19 30 l 1 sw s } def
    /_K { n 1 0 m 1 30 l 1 15 m 11 15 l 21 30 l 21 0 m 11 15 l 1 sw s } def
    /_L { n 1 30 m 1 0 l 21 0 l 21 2 l 1 sw s } def
    /_M { n 1 0 m 1 30 l 11 15 l 21 30 l 21 0 l 1 sw s } def
    /_N { n 1 0 m 1 30 l 21 15 l 21 30 m 21 0 l 1 sw s } def
    /_O { n 1 10 m 1 20 l 2 24 l 5 28 l 7 29 l 9 30 l 13 30 l 15 29 l
	  17 28 l 19 26 l 20 24 l 21 20 l 21 10 l 20 6 l 19 4 l 17 2 l
	  15 1 l 13 0 l 9 0 l 7 1 l 5 2 l 3 4 l 2 6 l 1 10 l 1 sw s } def
    /_P { n 1 0 m 1 30 l 14 30 l 17 29 l 19 28 l 20 26 l 21 24 l 21 21 l
	  20 19 l 19 17 l 17 16 l 14 15 l 1 15 l 1 sw s } def
    /_Q { n 1 10 m 1 20 l 2 24 l 3 26 l 5 28 l 7 29 l 9 30 l 13 30 l
	  15 29 l 17 28 l 19 26 l 20 24 l 21 20 l 21 10 l 20 6 l 19 4 l 
	  17 2 l 15 1 l 13 0 l 9 0 l 7 1 l 5 2 l 3 4 l 2 6 l 1 10 l 13 7 m
	  21 0 l 1 sw s } def
    /_R { n 1 0 m 1 30 l 14 30 l 17 29 l 19 28 l 20 26 l 21 24 l 21 21 l
	  20 19 l 19 17 l 17 16 l 14 15 l 1 15 l 21 0 m 11 15 l 1 sw s } def
    /_S { n 1 5 m 3 2 l 5 1 l 8 0 l 14 0 l 17 1 l 19 2 l 21 5 l 21 8 l
	  20 11 l 19 13 l 16 15 l 6 15 l 3 17 l 2 19 l 1 22 l 1 25 l 3 28 l
	  5 29 l 8 30 l 14 30 l 17 29 l 19 28 l 21 25 l 1 sw s } def
    /_T { n 1 28 m 1 30 l 21 30 l 21 28 l 11 30 m 11 0 l 9 0 m 13 0 l
	  1 sw s } def
    /_U { n 1 30 m 1 10 l 2 6 l 3 4 l 5 2 l 9 0 l 13 0 l 15 1 l 17 2 l
	  19 4 l 20 6 l 21 10 l 21 30 l 1 sw s } def
    /_V { n 1 30 m 11 0 l 21 30 l 1 sw s } def
    /_W { n 1 30 m 6 0 l 11 15 l 16 0 l 21 30 l 1 sw s } def
    /_X { n 1 0 m 21 30 l 1 30 m 21 0 l 1 sw s } def
    /_Y { n 11 0 m 11 15 l 1 30 l 11 15 m 21 30 l 9 0 m 13 0 l 1 sw s } def
    /_Z { n 1 28 m 1 30 l 21 30 l 1 0 l 21 0 l 21 2 l 1 sw s } def
    /_0 { n 3 4 m 2 6 l 1 10 l 1 20 l 2 24 l 3 26 l 5 28 l 7 29 l 9 30 l
	  13 30 l 15 29 l 17 28 l 19 26 l 20 24 l 21 20 l 21 10 l 20 6 l
	  19 4 l 17 2 l 15 1 l 13 0 l 9 0 l 7 1 l 5 2 l 3 4 l 18 27 l
	  1 sw s } def
    /_1 { n 4 0 m 18 0 l 11 0 m 11 30 l 4 24 l 1 sw s } def
    /_2 { n 1 22 m 1 25 l 3 27 l 5 29 l 8 30 l 14 30 l 17 29 l 19 27 l
	  21 25 l 21 22 l 20 19 l 19 17 l 1 0 l 21 0 l 1 sw s } def
    /_3 { n 1 22 m 1 25 l 3 27 l 5 29 l 8 30 l 14 30 l 17 29 l 19 27 l
	  21 25 l 21 22 l 20 19 l 18 17 l 16 15 l 11 15 l 16 15 m 18 13 l
	  20 11 l 21 8 l 21 5 l 19 3 l 17 1 l 14 0 l 8 0 l 5 1 l 3 3 l 1 5 l
	  1 8 l 1 sw s } def
    /_4 { n 16 0 m 16 30 l 1 10 l 21 10 l 1 sw s } def
    /_5 { n 1 5 m 3 2 l 5 1 l 8 0 l 14 0 l 17 1 l 19 2 l 21 5 l 21 8 l
	  20 11 l 19 13 l 16 15 l 1 15 l 1 30 l 21 30 l 1 sw s } def
    /_6 { n 21 30 m 15 30 l 11 29 l 9 28 l 7 27 l 5 25 l 3 22 l 2 19 l
	  1 15 l 1 3 l 2 2 l 5 1 l 8 0 l 14 0 l 17 1 l 19 2 l 21 5 l 21 8 l
	  20 11 l 19 13 l 16 15 l 6 15 l 3 14 l 2 12 l 1 9 l 1 sw s } def
    /_7 { n 1 28 m 1 30 l 21 30 l 11 15 l 11 0 l 1 sw s } def
    /_8 { n 6 15 m 3 17 l 2 19 l 1 22 l 1 25 l 3 28 l 5 29 l 8 30 l 14 30 l
	  17 29 l 19 28 l 21 25 l 21 22 l 20 19 l 19 17 l 16 15 l 6 15 l
	  3 13 l 2 11 l 1 8 l 1 5 l 3 2 l 5 1 l 8 0 l 14 0 l 17 1 l 19 2 l
	  21 5 l 21 8 l 20 11 l 19 13 l 16 15 l 1 sw s } def
    /_9 { n 1 0 m 7 0 l 11 1 l 13 2 l 15 3 l 17 5 l 19 8 l 20 11 l 21 15 l
	  21 27 l 20 28 l 17 29 l 14 30 l 8 30 l 5 29 l 3 28 l 1 25 l 1 22 l
	  2 19 l 3 17 l 6 15 l 16 15 l 19 17 l 20 19 l 21 21 l 1 sw s } def
    /_a { n 1 12 m 3 15 l 14 15 l 17 14 l 19 13 l 21 12 l 21 0 l 21 4 m
	  20 3 l 19 2 l 17 1 l 13 0 l 9 0 l 5 1 l 3 2 l 2 3 l 1 4 l 1 6 l
	  2 7 l 3 8 l 5 9 l 9 10 l 13 10 l 17 9 l 19 8 l 20 7 l 21 6 l
	  1 sw s } def
    /_b { n 1 0 m 1 10 l 2 12 l 3 13 l 5 14 l 9 15 l 13 15 l 17 14 l
	  19 13 l 20 12 l 21 10 l 21 5 l 20 3 l 19 2 l 17 1 l 13 0 l 9 0 l
	  5 1 l 3 2 l 2 3 l 1 5 l 1 10 m 1 30 l 1 sw s } def
    /_c { n 21 5 m 20 3 l 19 2 l 17 1 l 13 0 l 9 0 l 5 1 l 3 2 l 2 3 l
	  1 5 l 1 10 l 2 12 l 3 13 l 5 14 l 9 15 l 13 15 l 17 14 l 19 13 l
	  20 12 l 21 10 l 1 sw s } def
    /_d { n 21 30 m 21 5 l 20 3 l 19 2 l 17 1 l 13 0 l 9 0 l 5 1 l 3 2 l
	  2 3 l 1 5 l 1 10 l 2 12 l 3 13 l 5 14 l 9 15 l 13 15 l 17 14 l
	  19 13 l 20 12 l 21 10 l 21 5 m 21 0 l 1 sw s } def
    /_e { n 1 10 m 21 10 l 20 12 l 19 13 l 17 14 l 13 15 l 9 15 l 5 14 l
	  3 13 l 2 12 l 1 10 l 1 5 l 2 3 l 3 2 l 5 1 l 9 0 l 13 0 l 17 1 l
	  19 2 l 20 3 l 21 5 l 1 sw s } def
    /_f { n 11 0 m 11 25 l 12 28 l 13 29 l 15 30 l 18 30 l 19 29 l 20 28 l
	  21 25 l 8 15 m 14 15 l 1 sw s } def
    /_g { n 1 -10 m 2 -12 l 3 -13 l 5 -14 l 9 -15 l 13 -15 l 17 -14 l
	  19 -13 l 20 -12 l 21 -10 l 21 10 l 20 12 l 19 13 l 17 14 l
	  13 15 l 9 15 l 5 14 l 3 13 l 2 12 l 1 10 l 1 5 l 2 3 l 3 2 l
	  5 1 l 9 0 l 13 0 l 17 1 l 19 2 l 20 3 l 21 5 l 1 sw s } def
    /_h { n 1 0 m 1 30 l 1 10 m 2 12 l 3 13 l 5 14 l 9 15 l 13 15 l 17 14 l
	  19 13 l 20 12 l 21 10 l 21 0 l 1 sw s } def
    /_i { n 5 0 m 17 0 l 11 0 m 11 15 l 8 15 l 11 20 m 11 21 l 1 sw s } def
    /_j { n 1 -10 m 2 -12 l 4 -14 l 6 -15 l 9 -15 l 11 -14 l 13 -12 l
	  14 -10 l 14 15 l 12 15 l 14 20 m 14 21 l 1 sw s } def
    /_k { n 1 0 m 1 30 l 1 10 m 11 10 l 21 20 l 21 0 m 11 10 l 1 sw s } def
    /_l { n 6 0 m 16 0 l 11 0 m 11 30 l 7 30 l 1 sw s } def
    /_m { n 1 0 m 1 15 l 1 10 m 2 13 l 4 15 l 8 15 l 10 13 l 11 10 l
	  12 13 l 14 15 l 18 15 l 20 13 l 21 10 l 21 0 l 1 sw s } def
    /_n { n 1 0 m 1 15 l 1 10 m 2 12 l 3 13 l 5 14 l 9 15 l 13 15 l 17 14 l
	  19 13 l 20 12 l 21 10 l 21 0 l 1 sw s } def
    /_o { n 1 5 m 1 10 l 2 12 l 3 13 l 5 14 l 9 15 l 13 15 l 17 14 l
	  19 13 l 20 12 l 21 10 l 21 5 l 20 3 l 19 2 l 17 1 l 13 0 l 9 0 l
	  5 1 l 3 2 l 2 3 l 1 5 l 1 sw s } def
    /_p { n 1 10 m 2 12 l 3 13 l 5 14 l 9 15 l 13 15 l 17 14 l 19 13 l
	  20 12 l 21 10 l 21 5 l 20 3 l 19 2 l 17 1 l 13 0 l 9 0 l 5 1 l
	  3 2 l 2 3 l 1 5 l 1 15 m 1 -15 l 1 sw s } def
    /_q { n 21 5 m 20 3 l 19 2 l 17 1 l 13 0 l 9 0 l 5 1 l 3 2 l 2 3 l
	  1 5 l 1 10 l 2 12 l 3 13 l 5 14 l 9 15 l 13 15 l 17 14 l 19 13 l
	  20 12 l 21 10 l 21 5 l 19 -15 l 17 -15 m 21 -15 l 1 sw s } def
    /_r { n 1 0 m 1 15 l 1 10 m 2 12 l 3 13 l 5 14 l 9 15 l 13 15 l
	  17 14 l 19 13 l 20 12 l 21 10 l 1 sw s } def
    /_s { n 1 5 m 2 2 l 3 1 l 6 0 l 17 0 l 20 1 l 21 3 l 21 5 l 20 7 l
	  18 8 l 4 8 l 2 9 l 1 11 l 1 13 l 3 15 l 17 15 l 19 14 l 21 12 l
	  1 sw s } def
    /_t { n 11 30 m 11 3 l 12 1 l 14 0 l 16 0 l 18 1 l 19 3 l 6 20 m 16 20 l
	  1 sw s } def
    /_u { n 1 15 m 1 5 l 2 3 l 3 2 l 5 1 l 9 0 l 13 0 l 17 1 l 19 2 l 20 3 l
	  21 5 l 21 15 m 21 0 l 1 sw s } def
    /_v { n 1 15 m 11 0 l 21 15 l 1 sw s } def
    /_w { n 1 15 m 6 0 l 11 10 l 16 0 l 21 15 l 1 sw s } def
    /_x { n 1 0 m 21 15 l 1 15 m 21 0 l 1 sw s } def
    /_y { n 1 15 m 1 5 l 2 3 l 3 2 l 5 1 l 9 0 l 13 0 l 17 1 l 19 2 l
	  20 3 l 21 5 l 21 15 m 21 -10 l 20 -12 l 19 -13 l 17 -14 l
	  13 -15 l 9 -15 l 5 -14 l 3 -13 l 2 -12 l 1 -10 l 1 sw s } def
    /_z { n 1 13 m 1 15 l 21 15 l 1 0 l 21 0 l 21 2 l 1 sw s } def
    /_! { n 10 0 m 12 0 l 12 2 l 10 2 l 10 0 l 10 4 m 12 4 l 13 30 l 9 30 l
	  10 4 l 1 sw s } def
    /_" { n 9 26 m 9 30 l 13 26 m 13 30 l 1 sw s } def
    /_# { n 6 0 m 6 30 l 16 30 m 16 0 l 21 10 m 1 10 l 1 20 m 21 20 l
	  1 sw s } def
    /_$ { n 1 8 m 3 5 l 5 4 l 8 3 l 14 3 l 17 4 l 19 5 l 21 8 l 20 12 l
	  18 14 l 16 15 l 6 15 l 4 16 l 2 18 l 1 22 l 3 25 l 5 26 l 8 27 l
	  14 27 l 17 26 l 19 25 l 21 22 l 11 30 m 11 27 l 11 3 m 11 0 l
	  1 sw s } def
    /_percent { n 1 0 m 1 3 l 21 27 l 21 30 l 10 30 m 7 30 l 4 27 l 4 24 l
	  7 21 l 10 21 l 13 24 l 13 27 l 10 30 l 12 9 m 9 6 l 9 3 l 12 0 l
	  15 0 l 18 3 l 18 6 l 15 9 l 12 9 l 1 sw s } def
    /_& { n 19 15 m 21 15 l 20 15 m 20 8 l 19 4 l 18 2 l 15 0 l 7 0 l 4 1 l
	  2 2 l 1 4 l 1 9 l 2 11 l 3 12 l 4 13 l 11 15 l 16 16 l 18 17 l
	  19 19 l 20 22 l 19 25 l 18 27 l 16 29 l 14 30 l 10 30 l 8 28 l
	  7 26 l 7 23 l 8 20 l 20 1 l 21 2 l 20 1 m 19 0 l 1 sw s } def
    /_' { n 11 30 m 10 29 l 11 28 l 12 29 l 11 30 l 11 29 l 12 28 l 11 26 l
	  9 23 l 1 sw s } def
    /_lparen { n 18 30 m 16 29 l 11 26 l 8 23 l 5 19 l 4 15 l 5 11 l 8 7 l
	  11 4 l 16 1 l 18 0 l 1 sw s } def
    /_rparen { n 4 30 m 6 29 l 11 26 l 14 23 l 17 19 l 18 15 l 17 11 l
	  14 7 l 11 4 l 6 1 l 4 0 l 1 sw s } def
    /_* { n 7 11 m 15 19 l 11 20 m 11 10 l 15 11 m 7 19 l 6 15 m 16 15 l
	  1 sw s } def
    /_+ { n 11 10 m 11 20 l 6 15 m 16 15 l 1 sw s } def
    /_, { n 11 2 m 10 1 l 11 0 l 12 1 l 11 2 l 11 1 l 12 0 l 11 -2 l 9 -5 l
	  1 sw s } def
    /_- { n 6 15 m 16 15 l 1 sw s } def
    /_. { n 12 0 m 12 2 l 10 2 l 10 0 l 12 0 l 1 sw s } def
    /_slash { n 1 0 m 21 30 l 1 sw s } def
    /_: { n 11 0 m 11 1 l 11 15 m 11 16 l 1 sw s } def
    /_; { n 11 2 m 10 1 l 11 0 l 12 1 l 11 2 l 11 1 l 12 0 l 11 -2 l
	  9 -5 l 11 15 m 11 16 l 1 sw s } def
    /_lt { n 21 30 m 1 15 l 21 0 l 1 sw s } def
    /_= { n 16 12 m 6 12 l 6 18 m 16 18 l 1 sw s } def
    /_gt { n 1 0 m 21 15 l 1 30 l 1 sw s } def
    /_? { n 1 22 m 1 26 l 2 29 l 4 30 l 18 30 l 20 29 l 21 26 l 21 21 l
	  20 19 l 17 15 l 16 13 l 16 5 l 16 2 m 16 0 m 1 sw s } def
    /_@ { n 21 22 m 19 25 l 14 25 l 12 24 l 11 22 l 11 8 l 12 6 l 14 5 l
	  21 5 l 21 27 l 18 30 l 4 30 l 1 26 l 1 4 l 4 0 l 21 0 l 1 sw s } def
    /_lbracket { n 16 0 m 6 0 l 6 30 l 16 30 l 1 sw s } def
    /_backslash { n 1 30 m 21 0 l 1 sw s } def
    /_rbracket { n 6 0 m 16 0 l 16 30 l 6 30 l 1 sw s } def
    /_^ { n 1 20 m 11 30 l 21 20 l 1 sw s } def
    /__ { n 1 -5 m 21 -5 l 1 sw s } def
    /_` { n 14 23 m 12 26 l 10 28 l 10 29 l 11 30 l 12 29 l 11 28 l 11 29
	  l 1 sw s } def
    /_lbrace { n 16 0 m 12 1 l 11 3 l 11 10 l 10 13 l 6 15 l 10 17 l 11 20 l
	  11 27 l 12 29 l 16 30 l 1 sw s } def
    /_| { n 11 5 m 11 12 l 11 18 m 11 25 l 1 sw s } def
    /_rbrace { n 6 0 m 10 1 l 11 3 l 11 10 l 12 13 l 16 15 l 12 17 l 11 20 l
	  11 27 l 10 29 l 6 30 l 1 sw s } def
    /_~ { n 2 27 m 3 28 l 5 29 l 7 29 l 9 28 l 11 27 l 13 26 l 15 25 l
	  17 25 l 19 26 l 20 27 l 1 sw s } def
%%%    /_\200 { n 6 5 m 16 5 l 16 20 m 6 20 l 11 25 m 11 15 l 1 sw s } def
%%%    /_\201 { n 10 22 m 8 23 l 7 25 l 7 27 l 8 29 l 10 30 l 12 30 l
%%%	  14 29 l 15 27 l 15 25 l 14 23 l 12 22 l 10 22 l 1 sw s } def
%%%    /_\202 { n 14 0 m 11 1 l 9 2 l 6 4 l 5 5 l 3 8 l 2 10 l 1 13 l
%%%	  1 17 l 2 20 l 3 22 l 5 25 l 6 26 l 9 28 l 11 29 l 14 30 l 18 30 l
%%%	  21 29 l 23 28 l 26 26 l 27 25 l 29 22 l 30 20 l 31 17 l 31 13 l
%%%	  30 10 l 29 8 l 27 5 l 26 4 l 23 2 l 21 1 l 18 0 l 14 0 l 7 0 m
%%%	  25 30 l 1 sw s } def


  end

  /BuildChar
    { BuildCharDict begin
	/char exch def
	/fontdict exch def
	/charname fontdict /Encoding get
	  char get def
	/charproc fontdict /CharacterDefs
	  get charname get def
	30 0 -3 -18 23 33 setcachedevice
	gsave charproc grestore
      end
    } def

end
							   
/MyFont ExampleFont definefont pop
/basefont /MyFont findfont def
